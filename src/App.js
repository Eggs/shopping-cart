import React from "react";
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";

import NavBar from "./Components/NavBar";
import coffeeShop from "./images/coffee-shop.jpg";

const useStyles = makeStyles((theme) => ({
  link: {
    textDecoration: "none",
    color: "white",
  },
  button: {
    margin: "0",
    left: "10vw",
    top: "70vh",
    position: "absolute",
  },
  grid: {
    marginTop: "10px",
    height: "auto",
  },
  container: {
    width: "90%",
    height: "90vh",
    backgroundImage: "url(" + coffeeShop + ")",
    backgroundSize: "cover",
    marginTop: "12vh",
  },
  tagLine: {
    color: "white",
    width: "20vw",
  },
  tagContainer: {
    margin: "0",
    left: "10vw",
    top: "30vh",
    position: "absolute",
  },
}));

const App = (props) => {
  const classes = useStyles();

  return (
    <div>
      <NavBar basket={props.basket} />
      <Container id="content-container" className={classes.container}>
        <Grid container>
          <Grid item xs={3}>
            <div className={classes.tagContainer}>
              <Typography variant="h3" className={classes.tagLine}>
                LOCALLY SOURCED BEANS, ROASTED TO PERFECTION
              </Typography>
              <Link to="/shop" className={classes.link}>
                <Button
                  variant="contained"
                  startIcon={<ShoppingCartIcon />}
                  color="secondary"
                >
                  View our stock here
                </Button>
              </Link>
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default App;
