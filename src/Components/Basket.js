import React from "react";
import NavBar from "./NavBar";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import ToolBar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import ButtonGroup from "@material-ui/core/ButtonGroup";

import appleImage from "../images/apples.jpg";

const useStyles = makeStyles((theme) => ({
  card: {
    width: "250px",
    height: "auto",
    margin: "30px",
  },
  media: {
    height: 0,
    paddingTop: "99%", // 16:9
  },
  grid: {
    marginTop: "10px",
    marginBottom: "40px",
    justifyContent: "center",
    height: "auto",
  },
  button: {
    bottom: 0,
    padding: "2px",
    width: "100%",
  },
  productName: {
    padding: "7px",
  },
  buttonGroup: {
    width: "100%",
  },
  appBar: {
    top: "auto",
    bottom: 0,
  },
  appBarButton: {
    marginRight: "10px",
    marginLeft: "auto",
  },
  container: {
    width: "90%",
    height: "90vh",
    marginTop: "12vh",
  },
}));

const Basket = (props) => {
  const classes = useStyles();

  const calculateTotalCost = () => {
    let total = 0.0;
    if (props.basket.length > 0) {
      for (const i in props.basket) {
        Number(
          (total += props.basket[i].price * props.basket[i].count)
        ).toFixed(2);
      }
    } else {
      return "No items in basket";
    }
    return String("£" + Number(total).toFixed(2));
  };

  return (
    <div>
      <NavBar basket={props.basket} />
      <Container className={classes.container}>
        <Grid container className={classes.grid}>
          {props.basket.map((item) => {
            return (
              <Grid item key={props.basket.indexOf(item)}>
                <Card
                  className={classes.card}
                  key={props.basket.indexOf(item)}
                  id={props.basket.indexOf(item)}
                >
                  <CardMedia className={classes.media} image={appleImage} />
                  <CardHeader
                    title={
                      <div>
                        <Typography
                          className={classes.productName}
                          variant="h6"
                          color="textSecondary"
                        >
                          {item.name}
                        </Typography>
                        <Typography
                          className={classes.productName}
                          variant="body1"
                          color="textSecondary"
                        >
                          {"£" +
                            (Number(item.price) * Number(item.count)).toFixed(
                              2
                            )}
                        </Typography>
                      </div>
                    }
                    subheader={
                      <ButtonGroup
                        className={classes.buttonGroup}
                        variant="text"
                        color="primary"
                      >
                        <Button
                          key={props.basket.indexOf(item)}
                          id="add-to-basket"
                          variant="outlined"
                          color="secondary"
                          size="small"
                          className={classes.button}
                          onClick={(e) => {
                            const filtered = props.basket.filter(
                              (e) => e.name === item.name
                            );
                            const mergedObj = Object.assign({}, ...filtered);
                            console.log({ mergedObj });
                            console.log({ filtered });
                            if (item.count > 0) {
                              mergedObj.count -= 1;
                            }

                            const prevBasket = props.basket;

                            for (const e in prevBasket) {
                              if (prevBasket[e].name === mergedObj.name) {
                                console.log("Merged");
                                prevBasket.splice(e, 1, mergedObj);
                              }
                            }
                            props.setBasket([...prevBasket]);
                          }}
                        >
                          -
                        </Button>
                        <Button
                          className={classes.button}
                          key={props.basket.indexOf(item).count}
                        >
                          {item.count}
                        </Button>
                        <Button
                          className={classes.button}
                          variant="outlined"
                          onClick={(e) => {
                            const filtered = props.basket.filter(
                              (e) => e.name === item.name
                            );
                            const mergedObj = Object.assign({}, ...filtered);
                            console.log({ mergedObj });
                            console.log({ filtered });
                            mergedObj.count += 1;

                            const prevBasket = props.basket;

                            for (const e in prevBasket) {
                              if (prevBasket[e].name === mergedObj.name) {
                                console.log("Merged");
                                prevBasket.splice(e, 1, mergedObj);
                              }
                            }
                            props.setBasket([...prevBasket]);
                          }}
                        >
                          +
                        </Button>
                      </ButtonGroup>
                    }
                  />
                  <Button
                    variant="outlined"
                    color="secondary"
                    size="small"
                    className={classes.button}
                    onClick={(e) => {
                      const filtered = props.basket.filter(
                        (e) => e.name !== item.name
                      );
                      props.setBasket([...filtered]);
                    }}
                  >
                    Remove
                  </Button>
                </Card>
              </Grid>
            );
          })}
        </Grid>
        <AppBar position="fixed" className={classes.appBar} color="secondary">
          <ToolBar>
            <Typography variant="h6" className={classes.appBarButton}>
              {calculateTotalCost()}
            </Typography>
            <Button
              variant="contained"
              color="primary"
              className={classes.appBarButton}
            >
              Pay
            </Button>
          </ToolBar>
        </AppBar>
      </Container>
    </div>
  );
};

export default Basket;
