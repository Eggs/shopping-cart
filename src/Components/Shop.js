import React from "react";
import NavBar from "./NavBar";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";

import appleImage from "../images/apples.jpg";
import "./Shop.css";

const useStyles = makeStyles((theme) => ({
  paper: {
    height: "98vh",
    width: "100%",
  },
  card: {
    width: "250px",
    height: "auto",
    margin: "30px",
  },
  media: {
    height: 0,
    paddingTop: "99%", // 16:9
  },
  grid: {
    marginTop: "10px",
    justifyContent: "center",
    height: "auto",
  },
  button: {
    bottom: 0,
    padding: "2px",
    width: "100%",
  },
  container: {
    width: "90%",
    height: "90vh",
    marginTop: "12vh",
  },
}));

const Shop = (props) => {
  const classes = useStyles();

  const addItemEventListener = (e) => {
    let itemToAdd = "";

    switch (e.target.tagName) {
      case "SPAN":
        itemToAdd =
          props.itemsInStock[
            e.target.parentElement.parentElement.parentElement.parentElement
              .parentElement.id
          ];
        break;
      case "BUTTON":
        itemToAdd =
          props.itemsInStock[
            e.target.parentElement.parentElement.parentElement.parentElement.id
          ];
        break;
      default:
        break;
    }

    const filtered = props.basket.filter((e) => e.name === itemToAdd.name); // Check if item is already in basket.
    let mergedObj = {};
    const prevBasket = props.basket; // Create a copy of the basket.

    if (filtered.length === 0) {
      // If this item isn't already in the basket.
      mergedObj = { name: itemToAdd.name, count: 1, price: itemToAdd.price }; // Create new object with count of one and add to basket.
      prevBasket.push(mergedObj);
    } else {
      // If it is already in the basket then merge the items
      // to one Object and increase the item count by one.
      mergedObj = Object.assign({}, ...filtered);
      mergedObj.count += 1;
      for (const e in prevBasket) {
        // Remove the duplicates from the basket and add back the merged Object
        // with the updated product count.
        if (prevBasket[e].name === mergedObj.name) {
          prevBasket.splice(e, 1, mergedObj);
        }
      }
    }

    props.setBasket([...prevBasket]);
  };

  return (
    <div>
      <NavBar basket={props.basket} />
      <Container className={classes.container}>
        <Grid container spacing={3} className={classes.grid}>
          {props.itemsInStock.map((item) => {
            return (
              <Card
                className={classes.card ? classes.card : "card"}
                key={props.itemsInStock.indexOf(item)}
                id={props.itemsInStock.indexOf(item)}
              >
                <CardMedia className={classes.media} image={appleImage} />
                <CardHeader
                  title={
                    <div>
                      <Typography variant="h6" color="textSecondary">
                        {item.name}
                      </Typography>
                      <Typography variant="body1" color="textSecondary">
                        {"£" + Number(item.price).toFixed(2)}
                      </Typography>
                    </div>
                  }
                  subheader={
                    <Button
                      key={props.itemsInStock.indexOf(item)}
                      id="add-to-basket"
                      variant="outlined"
                      color="secondary"
                      size="small"
                      onClick={addItemEventListener}
                      className={classes.button}
                    >
                      Add to basket
                    </Button>
                  }
                />
              </Card>
            );
          })}
        </Grid>
      </Container>
    </div>
  );
};

export default Shop;
