import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Badge from "@material-ui/core/Badge";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: "#FFF",
    color: "black",
  },
  toolBar: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "right",
  },
  title: {
    flexGrow: 3,
  },
  link: {
    textDecoration: "none",
    color: "black",
  },
}));

const NavBar = (props) => {
  const classes = useStyles();

  const [numItemsInBasket, setNumItemsInBasket] = useState(0);

  useEffect(() => {
    setNumItemsInBasket(
      props.basket.length > 0
        ? props.basket.reduce((a, b) => {
            return { count: a.count + b.count };
          }).count
        : 0
    );
  }, [props.basket]);

  return (
    <AppBar className={classes.appBar} position="fixed">
      <Toolbar className={classes.toolBar}>
        <Typography variant="h6">
          <Link to="/" className={classes.link}>
            THE COFFEE SHOP
          </Link>
        </Typography>

        <Button variant="outlined">
          <Link to="/shop" className={classes.link}>
            SHOP
          </Link>
        </Button>

        <Link to="/basket">
          <Badge color={"secondary"} badgeContent={numItemsInBasket}>
            <Avatar variant="rounded">
              <ShoppingBasketIcon />
            </Avatar>
          </Badge>
        </Link>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
