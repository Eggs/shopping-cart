import React, { useState } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import App from "./App";
import Basket from "./Components/Basket";
import Shop from "./Components/Shop";

const Routes = () => {
  const [basket, setBasket] = useState([]);
  const [itemsInStock, setItemsInStock] = useState([
    { name: "Apples", price: 0.59 },
    { name: "Oranges", price: 0.7 },
    { name: "Nintendo Switch", price: 299.0 },
    { name: "Rubber Duck", price: 3.99 },
    { name: "Yukka", price: 40.0 },
    { name: "Book", price: 11.99 },
    { name: "Curtains", price: 14.99 },
    { name: "Laptop", price: 349.0 },
    { name: "Desk", price: 120.0 },
  ]);

  return (
    <BrowserRouter>
      <Switch>
        <Route
          path="/shop"
          component={props => (
            <Shop
              itemsInStock={itemsInStock}
              setItemsInStock={setItemsInStock}
              basket={basket}
              setBasket={setBasket}
              props={props}
            />
          )}
        />
        <Route path="/basket" component={props => <Basket basket={basket} setBasket={setBasket} props={props} />} />
        <Route path="/" component={props => <App basket={basket} props={props} />} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
